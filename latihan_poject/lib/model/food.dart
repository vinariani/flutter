import 'package:latihan_poject/database/database_provider.dart';
class Food{
   int productID;
  String productName;
  int price;
  int stock;

  Food({this.productID, this.productName, this.price, this.stock});

  Map<String, dynamic> toMap(){
     var map = <String, dynamic>{

       DatabaseProvider.COLUMN_Name: productName,
       DatabaseProvider.COLUMN_Price: price,
       DatabaseProvider.COLUMN_Stock: stock,

  
    };

    if(productID != null){
      map[DatabaseProvider.COLUMN_Id] = productID;
    }


    return map;
 
  }

   Food.fromMap(Map<String, dynamic> map) {
    productID = map[DatabaseProvider.COLUMN_Id];
    productName = map[DatabaseProvider.COLUMN_Name];
    price = map[DatabaseProvider.COLUMN_Price];
    stock = map[DatabaseProvider.COLUMN_Stock];
  }

}