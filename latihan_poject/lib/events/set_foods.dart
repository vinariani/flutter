import 'package:latihan_poject/model/food.dart';
import 'food_event.dart';
class SetFood extends FoodEvent {
  List<Food> foodList;

  SetFood(List<Food> foods) {
    foodList = foods;
  }
}