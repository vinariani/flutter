import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:latihan_poject/model/food.dart';

class DatabaseProvider{
  static const String TbProduct = "TbProduct";
  static const String COLUMN_Id = "ProductID;";
  static const String COLUMN_Name = "ProductName";
  static const String COLUMN_Price = "Price";
  static const String COLUMN_Stock = "Stock";

  DatabaseProvider._();
  static final DatabaseProvider db = DatabaseProvider._();
  
  Database _database;
  Future<Database> get database async{
    print("database getter caled");
    if(_database != null){
      return _database;
    }
    
    _database = await createDatabase();
      return _database;
  }
    

    Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();
    print(dbPath);

    return await openDatabase(
      join(dbPath, 'db_flutter.db'),
      version: 1,
      onCreate: (Database database, int version) async {
        print("Creating table");

        await database.execute(
          "CREATE TABLE $TbProduct ("
          "$COLUMN_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "$COLUMN_Name TEXT,"
          "$COLUMN_Price INTEGER,"
          "$COLUMN_Stock INTEGER"
          ")",
        );


        
      },
    );
  }


  Future<List<Food>> getFoods() async {
    final db = await database;

    var foods = await db
        .query(TbProduct, columns: [COLUMN_Id, COLUMN_Name, COLUMN_Price, COLUMN_Stock]);

    List<Food> foodList = List<Food>();

    foods.forEach((currentFood) {
      Food food = Food.fromMap(currentFood);

      foodList.add(food);
    });

    return foodList;
  }


  
}