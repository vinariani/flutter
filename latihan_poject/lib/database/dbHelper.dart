
import 'dart:io';

import 'package:latihan_poject/helper/food_helper.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
class DbHelper{
  static final DbHelper _instance = new DbHelper.internal();
  DbHelper.internal();

  factory DbHelper() => _instance;

  static Database _db;

  Future<Database> get db async{
    if(_db != null) return _db;
    _db = await setDb();
    return _db;
  }

  setDb() async
  {
    Directory directory= await getApplicationDocumentsDirectory();
    String path = join(directory.path, "dbflutter");

    var dB= await openDatabase(path, version:1, onCreate: _onCreate);
    return dB;

  }
  void _onCreate(Database db, int version) async{
    await db.execute("CREATE TABLE TbProduct(productID INTEGER PRIMARY KEY,productName TEXT, price TEXT, stock TEXT)");
    print("DATABASE BERHASIL DIBUAT");
  }

  //menyimpan data product ke database sqllite
  Future<int> insertProduct (FoodHelper foodhelper) async{//ngampil model food_helper
    var dbClient = await db;
    int response = await dbClient.insert("TbProduct", foodhelper.toMap());

    print("Data Berhasil disimpan");
    return response;


  }

  
  //tampilin data product
  Future<List<FoodHelper>> getDataProduct() async{
   var dbClient = await db;
   List<Map> list = await dbClient.rawQuery("SELECT * FROM TbProduct");
   List<FoodHelper> dataFood = List();
   for(int i =0; i<list.length;i++){
     var food = FoodHelper(list[i]["productName"], list[i]["price"], list[i]["stock"] );

     food.setIdFood(list[i]["productID"]);
     dataFood.add(food);

   }

   return dataFood;
  }
}