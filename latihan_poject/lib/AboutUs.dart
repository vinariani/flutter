//Import Package yang diperlukan


import 'package:flutter/material.dart';
import 'package:latihan_poject/homepage.dart';



class AboutUsPage extends StatefulWidget{
  
  @override
  _AboutUsPageState createState() =>  _AboutUsPageState();
}

//Class utama
class _AboutUsPageState extends  State<AboutUsPage> {
  
  void halamanUtama ()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => HomePage()));

  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Belajar Flutter',
      home: new Scaffold(
        //Membuat Widget AppBar
        appBar: new AppBar(
          //Menambahkan TitleBar
          title: new Text('About Us'),
          //Mengubah Warna Background
         backgroundColor: Colors.brown[500],
          //Menambahkan Leading menu
          leading: new IconButton(
            icon: new Icon(Icons.home, color: Colors.white),
            onPressed: ()=>halamanUtama(),
          ),
        ),

        body: Container(
            width:  MediaQuery.of(context).size.width,//untuk backgtound full hp
            padding: const EdgeInsets.all(8),
            
            decoration: BoxDecoration(
                gradient: LinearGradient(
                colors: [Colors.brown, Colors.grey],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter
            )
            ),
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.white,

                ),
                Image(image: AssetImage('image/logo.png')),
                Text("About Kopi Kenangan", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
                Text("Kopi Kenangan is the fastest growing non-franchise coffee chain in Indonesia. The idea of Kopi Kenangan started because the founders have the mission to spread their passion for Indonesian coffee. The Mission: to become the leading coffee chain in Indonesia and beyond by leveraging the ‘New Retail’ environment where the boundary between offline and online commerce disappears as we focus on fulfilling the personalized needs of each customer.The Vision: to become the biggest coffee chain in Indonesia and beyond (international expansion) through high quality yet affordable product, technology, fast & friendly service, quality control, and creative R&D.")
               

              ],
            ),

        ),
      ),
    );
  }
}