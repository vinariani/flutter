
import 'package:flutter/material.dart';

import 'homepage.dart';



void main()
{
  runApp(new  MaterialApp(
    home: new Login(),
  ));

}
class Login extends StatefulWidget{
  
  @override
  _LoginState createState() =>  _LoginState();
}
class _LoginState extends State<Login>{
  String  username = "Test";
  String  password ="11111";
  String alert ="Siap Login";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController usernameInput = new TextEditingController();
  TextEditingController passwordInput = new TextEditingController();
  void prosesLogin(){
    if(_formKey.currentState.validate()){
      if(usernameInput.text == username && passwordInput.text == password){       
             Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => HomePage(username: usernameInput.text,)));
      }else{      
           setState(() {
              alert="username dan password salah";
            });

      }

    }
  }
  @override
  Widget build (BuildContext context){
    return new Scaffold(
      body:  Container(
        width:  MediaQuery.of(context).size.width,//untuk backgtound full hp
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.brown, Colors.grey],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter
          )
        ),
        child: Column(
          mainAxisAlignment:  MainAxisAlignment.center,//biar ditengah
          children: <Widget>[//untuk membuat logo
            
            Image(image: AssetImage('image/logo.png')),
            SizedBox(height: 20,),

           
            Text(alert, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),

             Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                   TextFormField(//form login
                      controller: usernameInput,
                      validator: (value){
                        if(value.isEmpty){
                          return "Isi Ulang";
                        }
                        return null;

                      },
                        decoration:  InputDecoration(
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black87),
                          ),
                          prefixIcon:  Icon(Icons.person, size: 40,),
                          hintText: 'Masukan Username',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Username',
                          labelStyle: TextStyle(color: Colors.white),

                        ),
                      ),
                      SizedBox(height: 30,),

                    
                      TextFormField(//form password
                        controller: passwordInput,
                        validator: (value){
                            if(value.isEmpty){
                              return "Isi Password";
                            }
                            return null;

                      },
                        obscureText: true,//untuk hide 
                        decoration:  InputDecoration(
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black87),
                          ),
                          prefixIcon:  Icon(Icons.lock, size: 40,),
                          hintText: 'Masukan Password',
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: 'Password',
                          labelStyle: TextStyle(color: Colors.white),

                        ),
                      ),
                      SizedBox(height: 20,),

                      Card(//button 
                        color: Colors.black87,
                        elevation: 5,
                        child: Container(
                          height: 50,
                          child: InkWell(
                            splashColor: Colors.white,
                            onTap: () => prosesLogin(),
                            child: Center(
                              child: Text("Masuk", style: TextStyle(fontSize: 20, color: Colors.white),),
                            ),
                          ),
                        ),

                      ),
                ],
              ),
            ),
   
          ],
          ),


      ),

    );
  }
}
