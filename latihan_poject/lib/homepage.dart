
import 'package:flutter/material.dart';
import 'package:latihan_poject/AboutUs.dart';
import 'package:latihan_poject/ContactUs.dart';
import 'package:latihan_poject/helper/listfood.dart';
import 'package:latihan_poject/main.dart';



class HomePage extends StatefulWidget{
  final String username;
  HomePage({this.username});
  @override
  _HomePageState createState() =>  _HomePageState();
}

class  _HomePageState extends State<HomePage>{
  void aboutUs()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => AboutUsPage()));
  }
  void contactUs()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => ContactUsPage()));
  }
   void logout()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => Login()));
  }
   void foodlist()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => ListFood()));
  }
   
   @override
   Widget build(BuildContext context){
     return Scaffold(
      appBar: new AppBar(
        
         backgroundColor: Colors.brown[500],
         actions: <Widget>[
         
         ],
         title: Text("Kopi Kenangan",),
         
      ),
      
      drawer: new Drawer( //burger/sidebar
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader (
              accountName: new Text("Test"),
              accountEmail: new Text("Test@gmail.com"),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: new NetworkImage("https://source.unsplash.com/ZHvM3XI0HoE"),
              ),
             decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.brown, Colors.grey],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter
                )
              )
            ),
            new ListTile(
              title: new Text("About Us"),
              onTap: ()=> aboutUs(),
            ),
            new ListTile(
              title: new Text("Contact Us"),
               onTap: ()=> contactUs(),
            ),
            new ListTile(
              title: new Text("Logout"),
               onTap: ()=> logout(),
            ),
             new ListTile(
              title: new Text("foodlist"),
               onTap: ()=> foodlist(),
            )
          ],
        ),
      ),
   
      body: ListView(
         children: <Widget>[
           Akun(),
           Divider(),
           MenuUtama(),
          Divider(),
           
           Promo(),

         ], 
      ),
     );
   }

}


class Akun extends StatelessWidget{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      height: 150,
      width: 150,
     decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.brown, Colors.grey],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter
          )
        ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: ListTile(
          leading: Container(
            width: 80.0,
            height: 100.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage("https://source.unsplash.com/ZHvM3XI0HoE")
              ),
            ),
          ),
          title: Text(
            "Test", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
          ),
          subtitle: Row(
            children: <Widget>[
              RaisedButton.icon(
                icon: Icon(Icons.album),
                label: Text('0 Poin'),
                onPressed: (){},
                color: Colors.grey[200],
                elevation: 0.0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)), 
              )
            ],
          ),
        )
      ),
    );
  }
}

class MenuUtama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 4,
      children: menuUtamaItem,
      

      
    );
  }
}

List<MenuUtamaItem> menuUtamaItem = [
  MenuUtamaItem(
    title: 'Kopi',
    icon:  Icons.local_drink,
    colorBox: Colors.brown,

    
    
  ),
  MenuUtamaItem(
    title: 'Pulsa',
    icon: Icons.content_paste,
   colorBox: Colors.blue,
  ),
  MenuUtamaItem(
    title: 'Transfer',
    icon: Icons.calendar_today,
    colorBox: Colors.purple,
  ),
  MenuUtamaItem(
    title: 'My Profile',
    icon: Icons.face,
    colorBox: Colors.yellow,
  ),
  MenuUtamaItem(
    title: 'TopUp',
    icon: Icons.search,
    colorBox: Colors.grey,
  ),
   MenuUtamaItem(
    title: 'Settings',
    icon: Icons.settings,
    colorBox: Colors.pink,
  ),
  MenuUtamaItem(
    title: 'My Complaiment',
    icon: Icons.featured_play_list,
    colorBox: Colors.orange,
  ),
   MenuUtamaItem(
    title: 'More',
    icon: Icons.border_all,
    colorBox: Colors.green,
  ),
];

class MenuUtamaItem extends StatelessWidget {
  MenuUtamaItem({this.title, this.icon, this.colorBox, this.iconColor});
  final String title;
  final IconData icon;
  final Color colorBox, iconColor;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 50.0,
          width: 50.0,
          decoration: BoxDecoration(
            color: colorBox,
            shape: BoxShape.circle,
          ),
          child: Icon(icon, color: iconColor,),
          
        ),
        Text(title, style: TextStyle(fontSize: 12),)
      ],


      
    );
  }
}


    

class Promo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Promo Saat Ini", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
          trailing: IconButton(
            icon: Icon(Icons.keyboard_arrow_right),
            onPressed: (){},
            ),

        ),

        Container(
          width: double.infinity,
          height: 150,
          padding: EdgeInsets.only(left: 8),
          child: ListView(//buat gambar
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              
                    
              //untuk gambar 
              Container(
                height: 150,
                width:400,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: AssetImage('image/4.jpg')
                  )
                ),
                margin: EdgeInsets.only(left: 10),
                child: null,
              ),
              Container(
                height: 150,
                width:150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: AssetImage('image/1.jpg')
                  )
                ),
                margin: EdgeInsets.only(left: 10),
                child: null,
              ),
              Container(
                height: 150,
                width:150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: AssetImage('image/2.jpg')
                  )
                ),
                margin: EdgeInsets.only(left: 10),
                child: null,
              ),
              Container(
                height: 150,
                width:150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: AssetImage('image/3.jpg')
                  )
                ),
                margin: EdgeInsets.only(left: 10),
                child: null,
              )





            ],
          ),
        )
      ],
      
    );
  }
}

