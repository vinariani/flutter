//Import Package yang diperlukan
import 'package:flutter/material.dart';

import 'homepage.dart';

class ContactUsPage extends StatefulWidget{
  
  @override
  _ContactUsPage createState() =>  _ContactUsPage();
}

class _ContactUsPage extends State<ContactUsPage> {
   
  void halamanUtama ()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => HomePage()));

  }
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Contact Us',
      home: new Scaffold(
        //Membuat Widget AppBar
        appBar: new AppBar(
          //Menambahkan TitleBar
          title: new Text('Contact Us'),
          //Mengubah Warna Background
          backgroundColor: Colors.brown[500],
          //Menambahkan Leading menu
          leading: new IconButton(
            icon: new Icon(Icons.home, color: Colors.white),
            onPressed: ()=>halamanUtama(),
          ),
         
        ),

        body: Container(
         
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
               Image(image: AssetImage('image/logo.png')),
               SizedBox(height: 20,),

              Text("Contact Us:", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
              Text("kopikenangan@gmail.com", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),
              

            ],
           
          ),

        ),
      ),
    );
  }
}