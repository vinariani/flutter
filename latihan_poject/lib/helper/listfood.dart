//Import Package yang diperlukan


import 'package:flutter/material.dart';
import 'package:latihan_poject/database/dbHelper.dart';
import 'package:latihan_poject/helper/food_helper.dart';
import 'package:latihan_poject/helper/insert_food.dart';
import 'package:latihan_poject/homepage.dart';
class ListFood extends StatefulWidget{
  @override
  ListFoodState createState() => ListFoodState();
}

//Class utama
class ListFoodState extends State<ListFood>{
  void home()
  {
    Navigator.pushReplacement(context, MaterialPageRoute
            (builder: (context) => HomePage()));
  }

  var db = DbHelper();
  @override
  Widget build(BuildContext context) {
    return  Scaffold(

        //Membuat Widget AppBar
        appBar: new AppBar(
          //Menembahkan Title/Judul
          actions: <Widget>[
            IconButton(icon: Icon(Icons.home), onPressed: ()=>home())
          ],
          title: new Text('List Food'),
          backgroundColor: Colors.brown[500],
        ),

        body: FutureBuilder(
          future: db.getDataProduct(),
          builder: (context, snapshot){
            if(snapshot.hasError)print(snapshot.error);
            var data = snapshot.data;
            return snapshot.hasData
            ? new ListFoodTampilan(data)
            : Center(
              child: Text("Tidak adaProduct"),
            );


          },

        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.camera, color: Colors.brown,),
          onPressed: (){
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context)=> InsertFood())

            );

          },
        )

        
      
    );
  }
}

class ListFoodTampilan extends StatefulWidget {
  final List<FoodHelper> list;
  ListFoodTampilan(this.list);
  @override
  _ListFoodTampilanState createState() => _ListFoodTampilanState();
}

class _ListFoodTampilanState extends State<ListFoodTampilan>
{
   
 
  @override
  Widget build(BuildContext context) {
    return Container(
      padding:  const EdgeInsets.all(8),
      width: MediaQuery.of(context).size.width,
      child: GridView.count(
        crossAxisCount: 2,
        scrollDirection: Axis.vertical,
        children: List.generate(widget.list == null ? 0 : widget.list.length,
         (index){
          
          return Card(
            child: Container(
               padding:  const EdgeInsets.all(4),
               child: ListView(
                 children: <Widget>[
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: <Widget>[
                       IconButton(icon: Icon(Icons.edit), onPressed: null),
                        IconButton(icon: Icon(Icons.delete), onPressed: null)
                     ],

                   ),

                   SizedBox(height: 4,),

                   Text(widget.list[index].productName),
                   SizedBox(height: 4,),

                    Text(widget.list[index].price),
                   SizedBox(height: 4,),

                    Text(widget.list[index].stock),
                   SizedBox(height: 4,),
                 ],
               )

            ),
          
           

          );
         }
         ),
        ),
    );
  }
}