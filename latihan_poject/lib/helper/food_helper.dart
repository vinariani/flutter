class FoodHelper{
  int id;
  String nama;
  String _price;
  String _stock;

  FoodHelper(this.nama, this._price, this._stock);

  FoodHelper.map(dynamic obj){
    this.nama= obj["ProductName"];
    this._price= obj["Price"];
    this._stock = obj["Stock"];
  }

  String get productName => nama;
  String get price => _price;
  String get stock => _stock;

  Map<String, dynamic> toMap(){
    var mapFood = Map<String, dynamic>();
    mapFood["productName"] = productName;
    mapFood["price"] = _price;
    mapFood["stock"] = _stock;
    
    return mapFood;

  }

  void setIdFood(int id){
    this.id = id;
  }

}