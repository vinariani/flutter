// import 'dart:io';
// import 'dart:async';



import 'package:flutter/material.dart';
import 'package:latihan_poject/database/dbHelper.dart';
import 'package:latihan_poject/helper/food_helper.dart';

// import 'package:image_picker/image_picker.dart';
  
class InsertFood extends StatefulWidget {
  @override
  _InsertFoodState createState() => _InsertFoodState();
}

class _InsertFoodState extends State<InsertFood>{
  //File _foto;

//  void fotoGalery() async{
//    var foto = await ImagePicker.pickImage(source: ImageSource.camera);
//    setState(() {
//      _foto = foto;
//    });
//  } 

  //untukinsert product
  final nameProduct = new TextEditingController();
  final priceProduct = new TextEditingController();
  final stockProduct = new TextEditingController();

  
  void uploadProduct() async{
    var db = DbHelper();
    
    var dataFood = FoodHelper( nameProduct.text, priceProduct.text, stockProduct.text);
    await db.insertProduct(dataFood);
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          //Menembahkan Title/Judul
          title: new Text('Insert Food'),
          backgroundColor: Colors.brown[500],
          actions: <Widget>[
            // IconButton(icon: Icon(Icons.add_a_photo), onPressed: (){
            //   fotoGalery();
            // }),
          ],
        ),
      body: Container(
        padding: const EdgeInsets.all(8),
        child: ListView(
          children: <Widget>[
          
            TextFormField(
              controller: nameProduct,
              decoration:  InputDecoration(
                labelText: "Name",
                hintText: "Masukan Nama Product",
                border: OutlineInputBorder()
              ),
            ),
             SizedBox(height: 30,),
             TextFormField(
              controller: priceProduct,
              decoration:  InputDecoration(
                labelText: "Price",
                hintText: "Masukan Price Product",
                border: OutlineInputBorder()
              ),
            ),
             SizedBox(height: 30,),

             TextFormField(
              controller: stockProduct,
              decoration:  InputDecoration(
                labelText: "Stock",
                hintText: "Masukan Stock Product",
                border: OutlineInputBorder()
              ),
            ),
             SizedBox(height: 30,),
             Card(
               elevation: 0,
               color: Colors.grey,
               child: Container(
                 height: 50,
                 child: InkWell(
                   splashColor: Colors.white,
                   child: Center(
                     child: Text("Upload Product")
                   ),
                   onTap: (){uploadProduct();

                   },
                 ),
               ),

             ),
          ],
        ),
      ),
      
    );
  }
}